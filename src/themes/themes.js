// dark theme
export const darkTheme = {
	body: 'hsl(207, 26%, 17%)',
	background: 'hsl(207, 26%, 17%)',
	inputBackground: 'hsl(209, 23%, 22%)',
	cardBackground: 'hsl(209, 23%, 22%)',
	themeButtonBackground: 'hsl(207, 26%, 17%)',
	textColor: 'hsl(0, 0%, 100%)',
	themeButtonColor: 'hsl(0, 0%, 100%)'
};
// light theme
export const lightTheme = {
	body: 'hsl(0, 0%, 98%)',
	background: 'hsl(0, 0%, 98%)',
	inputBackground: 'hsl(0, 0%, 52%)',
	cardBackground: 'hsl(0, 0%, 100%)',
	themeButtonBackground: 'hsl(0, 0%, 98%)',
	textColor: 'hsl(200, 15%, 8%)',
	themeButtonColor: 'hsl(200, 15%, 8%)'
};
